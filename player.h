#ifndef PLAYER_H
#define PLAYER_H
#include <QGraphicsEllipseItem>
#include <QObject>
#include <QBrush>
class player : public QObject,public QGraphicsEllipseItem{
    Q_OBJECT
public:
    player(int x,int y);
    int getVida();
    int getX();
    int getY();
    void hitted();

private:
    int vida = 1;
    int x,y;
};

#endif // PLAYER_H
