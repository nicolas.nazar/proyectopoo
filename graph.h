#ifndef GRAPH_H
#define GRAPH_H

#include <QGraphicsLineItem>
#include <QObject>

class graph : public QObject, public QGraphicsLineItem{
public:
    graph(int x,int y,int x2,int y2);
};

#endif // GRAPH_H
