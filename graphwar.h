#ifndef GRAPHWAR_H
#define GRAPHWAR_H
#include <QGraphicsScene>
#include <QMainWindow>
#include "graph.h"
#include "player.h"
#include <QMessageBox>
#include "playerred.h"
#include "playerblue.h"
#include <QRandomGenerator>

QT_BEGIN_NAMESPACE
namespace Ui { class graphwar; }
QT_END_NAMESPACE

class graphwar : public QMainWindow{
    Q_OBJECT

public:
    graphwar(QWidget *parent = nullptr);
    ~graphwar();
    void attack(player *jugador,player *jugador2,int pos);
public slots:
    void turnos();
    

private:
    Ui::graphwar *ui;
    QGraphicsScene *scene;
    graph *linea;
    playerRed *player1;
    playerBlue *player2;
    int turno = 1;

};
#endif // GRAPHWAR_H
